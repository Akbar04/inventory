@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-3"></div>
    <div class="column is-auto">
        <div class="card">
            <header class="card-header">
                <h1 class="card-header-title">
                    <a href="{{ URL::previous() }}" type="button" class="button is-info">
                        <span class="icon"><i class="fas fa-arrow-left"></i></span>
                    </a>
                    &nbsp;
                    Inventory
                </h1>

            </header>
            <div class="card-content">

                {!! Form::open(['url' => 'item']) !!}
                <div class="field">
                    {!! Form::label('name', 'Nama Barang',['class' => 'label']); !!}
                    <div class="control">
                        {!! Form::text('name', '',['class' => 'input']); !!}
                    </div>
                    <p class="help is-danger">{{ ($errors -> has('name')) ? $errors->first('name') : '' }}</p>
                </div>

                <div class="field">
                    {!! Form::label('brand', 'Merek Barang',['class' => 'label']); !!}
                    <div class="control">
                        {!! Form::text('brand', '',['class' => 'input']); !!}
                    </div>
                    <p class="help is-danger">{{ ($errors -> has('brand')) ? $errors->first('brand') : '' }}</p>
                </div>

                <div class="field">
                    {!! Form::label('many', 'jumlah Barang',['class' => 'label']); !!}
                    <div class="control">
                        {!! Form::number('many', '',['class' => 'input']); !!}
                    </div>
                    <p class="help is-danger">{{ ($errors -> has('many')) ? $errors->first('many') : '' }}</p>
                </div>

                <div class="field">
                    <div class="control">
                        {!! Form::select('condition', ['Bagus' => 'Bagus', 'Rusak' => 'Rusak'], null, ['placeholder' => 'Kondisi Barang', 'class' => 'select']); !!}
                    </div>
                </div>

                <div class="field">
                    {!! Form::label('detail', 'Detail Barang',['class' => 'label']); !!}
                    <div class="control">
                        {!! Form::textarea('detail', '',['class' => 'textarea']); !!}
                    </div>
                    <p class="help is-danger">{{ ($errors -> has('detail')) ? $errors->first('detail') : '' }}</p>
                </div>

                <div class="field">
                    <div class="control">
                        {!! Form::submit('SUBMIT',['class'=>'button']); !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="column is-3"></div>
</div>
@endsection
