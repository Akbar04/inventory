@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-1"></div>
    <div class="column is-auto">

        @if(Session::has('message'))
        <div class="notification is-primary">
            <button class="delete" aria-label="close"></button>
            {{ Session::get('message') }}
        </div>
        @endif

        <div class="card">
            <header class="card-header">
                <h1 class="card-header-title">
                    Inventory
                    &nbsp;
                    <a href="{{ route('item.create') }}" class="button is-primary is-pulled-right">
                        <span class="icon"><i class="fas fa-plus"></i></span>
                    </a>
                </h1>
            </header>
            <div class="card-content">
                <table class="table is-fullwidth">
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Merek</th>
                        <th>Banyak</th>
                        <th>Kondisi</th>
                        <th colspan="3">Keterangan</th>
                    </tr>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->brand }}</td>
                            <td>{{ $item->many }}</td>
                            <td>{{ $item->condition }}</td>
                            <td> <a href="{{ route('item.edit',$item->id) }}" type="button" class="button is-warning">
                                    <span class="icon"><i class="fas fa-edit"></i></span>
                                </a>
                            </td>
                            <td> <a href="{{ route('item.show',$item->id) }}" type="button" class="button is-info">
                                    <span class="icon"><i class="fas fa-eye"></i></span>
                                </a>
                            </td>
                            <td>
                                <button   type="button" name="button" class="button is-danger deleteButton modal-button"  data-target="modal-confirm-delete-{{$item->id}}">
                                    <span class="icon"><i class="fas fa-trash-alt"></i></span>
                                </button>
                                <div class="modal" id="modal-confirm-delete-{{$item->id}}">
                                    <div class="modal-background"></div>
                                    <div class="modal-card is-radiusless">
                                        <header class="modal-card-head is-radiusless">
                                            <p class="modal-card-title">Konfirmasi Penghapusan Data Inventory</p>
                                            <button class="delete" aria-label="close"></button>
                                        </header>

                                        <section class="modal-card-body">
                                            <p class="has-text-center">Apakah Anda Yakin Ingin Menghapus Data Inventory ini ?</p>
                                        </section>

                                        <footer class="modal-card-foot is-flex" style="justify-content: flex-end;">

                                            <a class="button is-success is-radiusless button-close-modal">
                                                <span class="icon is-large">
                                                    <i class="fas fa-window-close" aria-hidden="true"></i>
                                                </span>
                                                &nbsp;
                                                Batal
                                            </a>

                                            <form action="{{ route('item.destroy',$item->id) }}" method="post">
                                                {{ method_field('DELETE') }}
                                                @csrf

                                                <button class="button is-danger" type="submit">
                                                    <span class="icon is-large">
                                                        <i class="fas fa-trash" aria-hidden="true"></i>
                                                    </span>
                                                    &nbsp;
                                                    Konfirmasi
                                                </button>
                                            </form>
                                        </footer>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <footer>
                {{ $items->links('vendor.pagination.bulma') }}
            </footer>
        </div>
    </div>
    <div class="column is-1"></div>
</div>
@endsection
@section('javascript')
<script>
document.addEventListener('DOMContentLoaded', function () {
    function getAll(selector) {
        return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
    }
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {
                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);
                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
    // modal handler
    // Modals
    var rootEl = document.documentElement;
    var $modals = getAll('.modal');
    var $modalButtons = getAll('.modal-button');
    var $modalCloses = getAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button-close-modal');
    if ($modalButtons.length > 0) {
        $modalButtons.forEach(function ($el) {
            $el.addEventListener('click', function () {
                var target = $el.dataset.target;
                var $target = document.getElementById(target);
                rootEl.classList.add('is-clipped');
                $target.classList.add('is-active');
            });
        });
    }

    if ($modalCloses.length > 0) {
        $modalCloses.forEach(function ($el) {
            $el.addEventListener('click', function () {
                closeModals();
            });
        });
    }

    document.addEventListener('keydown', function (event) {
        var e = event || window.event;
        if (e.keyCode === 27) {
            closeModals();
            closeDropdowns();
        }
    });

    function closeModals() {
        rootEl.classList.remove('is-clipped');
        $modals.forEach(function ($el) {
            $el.classList.remove('is-active');
        });
    }
    // Dropdowns
    var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
    if ($dropdowns.length > 0) {
        $dropdowns.forEach(function ($el) {
            $el.addEventListener('click', function (event) {
                event.stopPropagation();
                $el.classList.toggle('is-active');
            });
        });
        document.addEventListener('click', function (event) {
            closeDropdowns();
        });
    }
    function closeDropdowns() {
        $dropdowns.forEach(function ($el) {
            $el.classList.remove('is-active');
        });
    }
});

$(document).on('click', '.notification > button.delete', function() {
    $(this).parent().addClass('is-hidden');
    return false;
});
</script>
@endsection
