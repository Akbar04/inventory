@extends('layouts.app')

@section('content')

<div class="columns">
    <div class="column is-1"></div>
    <div class="column is-auto">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    <a href="{{ URL::previous() }}" type="button" class="button is-info">
                        <span class="icon"><i class="fas fa-arrow-left"></i></span>
                    </a>
                    &nbsp;
                    Data Barang
                </p>
            </header>
            <div class="card-content">
                <div class="content">
                    <table class="table is-fullwidth">
                        <tr>
                            <th>Nama Barang</th>
                            <td>: {{ $items->name }}</td>
                        </tr>
                        <tr>
                            <th>Merek Barang</th>
                            <td>: {{ $items->brand }}</td>
                        </tr>
                        <tr>
                            <th>Jumlah Barang</th>
                            <td>: {{ $items->many }}</td>
                        </tr>
                        <tr>
                            <th>Kondisi Barang</th>
                            <td>: {{ $items->condition }}</td>
                        </tr>
                        <tr>
                            <th>Detail</th>
                            <td>: {{ $items->detail }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="column is-1"></div>
</div>



@endsection

@section('footer')
@include('layouts.footer')
@endsection
