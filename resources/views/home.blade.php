@extends('layouts.app')

@section('content')
<div class="columns">
  <div class="column is-1"></div>
  <div class="column is-auto">
    <div class="card">
      <header class="card-header">
        <h1 class="card-header-title"> Inventory </h1>
      </header>
      <div class="card-content">
        <table class="table">
        <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Banyaknya</th>
          <th colspan="3">Keterangan</th>
        </tr>
        <?php foreach ($items as $item): ?>
          <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->many }}</td>
            <td> <a href="#" type="button" class="button is-link">Edit</a> </td>
            <td> <a href="#" type="button" class="button is-danger">Delete</a> </td>
            <td> <a href="#" type="button" class="button is-info">Detail</a> </td>
          </tr>
        <?php endforeach; ?>
      </table>
      </div>
    </div>
  </div>
  <div class="column is-1"></div>
</div>
@endsection
