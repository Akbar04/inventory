<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bulma.css') }}" rel="stylesheet">

    <!-- Styles -->


</head>
<body>
    <nav class="navbar is-dark" data-bulma="navbar">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ url('/') }}">
                <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
            </a>
            <div class="navbar-burger burger" data-target="navMenuExample" data-trigger>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div id="navMenuExample" class="navbar-menu">
            @guest
            <div class="navbar-start is-pulled-right">

            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="field is-grouped">
                        <p class="control">
                            <a class="navbar-item has-text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </p >
                        <p class="control">
                            <a class="navbar-item has-text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </p>
                    </div>
                </div>
            </div>
            @else
            <div class="navbar-start">
                <a class="navbar-item has-text-white" href="{{ route('item.index') }}">
                    Home
                </a>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link has-text-white" href="/documentation/overview/start/">
                        Docs
                    </a>
                    <div class="navbar-dropdown is-boxed">

                    </div>
                </div>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="field is-grouped">
                        <p class="control">
                            <li class="navbar-item has-dropdown is-hoverable">
                                <a id="navbarDropdown" class="navbar-link" href="#"  v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="navbar-dropdown is-boxed" aria-labelledby="navbarDropdown">
                                    <a class="navbar-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </p >
                </div>
            </div>
        </div>
    </div>
</nav>
<br>
<main>

    @yield('content')
</main>
<br>
<footer>
    @include('layouts.footer')
</footer>
<script src="{{ asset('js/bulma.js') }}"></script>
<script src="{{ asset('js/modal.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script defer src="{{ asset('js/fontawesome-all.js') }}"></script>
@section('javascript')
@show
</body>
</html>
