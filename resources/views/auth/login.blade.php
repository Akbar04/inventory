@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <header class="card-header">
                <div class="card-header-title">{{ __('Login') }}</div>
              </header>


                <div class="card-content">
                    <form class="content" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="yield">
                            <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                            <div class="control">
                              <div class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required autofocus>
                              </div>


                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="yield">
                            <label for="password" class="label">{{ __('Password') }}</label>

                            <div class="control">
                              <div class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                <input id="password" type="password" class="input" name="password" required>
                              </div>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="fieldd">
                            <div class="control">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="button is-success">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
