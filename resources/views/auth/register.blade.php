@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <header class="card-header">
                <div class="card-header-title">{{ __('Register') }}</div>
              </header>

                <div class="card-content">
                    <form class="content" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="field">
                            <label for="name" class="label">{{ __('Name') }}</label>

                            <div class="control">
                              <div class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
                                <input id="name" type="text" class="input" name="name" value="{{ old('name') }}" required autofocus>
                              </div>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong class="input is-danger">{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                            <div class="control">
                              <div class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required>
                              </div>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <label for="password" class="label">{{ __('Password') }}</label>

                            <div class="control">
                              <div class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                <input id="password" type="password" class="input" name="password" required>
                              </div>


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field">
                            <label for="password-confirm" class="label">{{ __('Confirm Password') }}</label>

                            <div class="control">
                              <div class="form-control">
                                <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                              </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="button is-success">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
  @include('layouts.footer')
@stop
