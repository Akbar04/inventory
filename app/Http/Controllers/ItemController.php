<?php declare(strict_types=1);

namespace App\Http\Controllers;

use DB;
use App\Item as Model;
use Illuminate\Http\Request;
use App\Http\Requests\CreateItem;

class ItemController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $model = 'model';
        $items = Model::orderBy('id', 'asc')->paginate(5);
        return view('item.index', compact('model','items'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('item/create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(CreateItem $request)
    {
        $model = new Model();
        $model->name      = $request->get('name');
        $model->many      = $request->get('many');
        $model->brand     = $request->get('brand');
        $model->condition = $request->get('condition');
        $model->detail    = $request->get('detail');
        $model->save();
        return redirect()->route('item.index')
        ->with('message', 'Data Berhasil Tersimpan');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $model = 'model';
        $items = Model::findOrFail($id);
        return view('item\show', compact('model','items'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $model = 'model';
        $item  = Model::findOrFail($id);
        return view('item\edit', compact('model','item'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(CreateItem $request, Model $item)
    {
        $item->name      = $request->get('name');
        $item->many      = $request->get('many');
        $item->brand     = $request->get('brand');
        $item->condition = $request->get('condition');
        $item->detail    = $request->get('detail');
        $item->save();
        return redirect()->route('item.index')
        ->with('message', 'Data Berhasil Tersimpan');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $items = Model::findOrFail($id);
        $items->delete();
        return redirect()->route('item.index');
    }
}
