<?php

namespace App\Providers;

use Request;
use Illuminate\Support\ServiceProvider;

class LaravelAppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $halaman = '';
        if(Request::segment(1) =='Home'){
            $halaman = 'Home';
        }
        view()->share('halaman',$halaman);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
